<?php

/**
 * @link              https://gitlab.com/puikepixels/wp-custom-posttype-portfolio
 * @since             1.0.0
 * @package           WP_Cpt
 * @wordpress-plugin
 * Plugin Name:       Puikepixels WP Custom Posttype portfolio.
 * Plugin URI:        https://gitlab.com/puikepixels/wp-custom-posttype-portfolio
 * Description:       This is a WordPress Custom Post Type Plugin boilerplate. It makes creating custom post types very easy and saves a lot of time. It is properly documented so it is easier for you to customize it as per your requirements.
 * Version:           1.0.0
 * Author:            Bram Wientjes
 * Author URI:        https://gitlab.com/puikepixels
 * License:           GPL-2.0+
 * License URI:       http://gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'WP_CPT_VERSION', '1.0.0' );

/**
 * Includes the file containing Custom Post Type code.
 * @since 1.0.0
 * @uses require_once()
 */

require_once( dirname( __FILE__ ) . '/inc/cpt.php' );